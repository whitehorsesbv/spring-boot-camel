package nl.whitehorses.camel;

import com.mongodb.MongoClient;
import nl.whitehorses.camel.beans.OrderBean;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.util.Collections;

/**
 * Entry point of the Spring Boot application. This class will also be used to register the required {@link Bean}
 * instances.
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    /**
     * The {@code camelServletBean} will be used to register the {@code CamelServlet}, including the mapping for the
     * URL pattern {@code /camel/*}. This servlet will be used for the REST DSL in the {@link CamelRouter}.
     *
     * @return {@link ServletRegistrationBean} instance of the {@code CamelServlet}.
     */
    @Bean
    @SuppressWarnings("unchecked")
    public ServletRegistrationBean camelServletBean() {
        final ServletRegistrationBean servlet = new ServletRegistrationBean(new CamelHttpTransportServlet(), "/camel/*");
        servlet.setName("CamelServlet");
        servlet.setLoadOnStartup(1);
        return servlet;
    }

    /**
     * The {@code activeMqConnectionFactoryBean} will be used to register an ActiveMQ connection factory. The broker URL
     * will be resolved via the {@code activemq.broker.url} application property. Also, the package {@code nl.whitehorses.camel}
     * will be marked as a trusted package.
     *
     * @param brokerUrl ActiveMQ broker URL, this will be automatically injected from the {@code application.properties}.
     * @return {@link ActiveMQConnectionFactory} instance.
     */
    @Bean
    public ActiveMQConnectionFactory activeMqConnectionFactoryBean(@Value("${activemq.broker.url}") final String brokerUrl) {
        final ActiveMQConnectionFactory activeMqConnectionFactory = new ActiveMQConnectionFactory();
        activeMqConnectionFactory.setBrokerURL(brokerUrl);
        activeMqConnectionFactory.setTrustedPackages(Collections.singletonList("nl.whitehorses.camel"));
        return activeMqConnectionFactory;
    }

    /**
     * The {@code mongoClientBean} will be used for connecting to a MongoDB database from the {@link CamelRouter}. The
     * Mongo client host and port will be resolved via the {@code mongodb.host} and {@code mongodb.port} application
     * properties.
     *
     * @param host MongoDB host, this will be automatically injected from the {@code application.properties}.
     * @param port MongoDB port, this will be automatically injected from the {@code application.properties}.
     * @return {@link MongoClient} instance.
     */
    @Bean
    public MongoClient mongoClientBean(@Value("${mongodb.host}") final String host, @Value("${mongodb.port}") final int port) {
        return new MongoClient(host, port);
    }

    /**
     * Expose {@link OrderBean} instance as {@code orderBean}.
     *
     * @return {@link OrderBean} instance}
     */
    @Bean
    public OrderBean orderBean() {
        return new OrderBean();
    }

    /**
     * Main method to run the Spring application.
     *
     * @param args Arguments.
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
