package nl.whitehorses.camel.beans;

import nl.whitehorses.camel.model.Order;
import nl.whitehorses.camel.model.OrderLine;
import org.apache.camel.Body;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Implementation of the {@code orderBean}. This bean contains the logic for transforming an XML representation of an
 * order, to the (canonical) JSON model.
 */
public class OrderBean {

    /**
     * Implementation of the transformation of an XML representation of an order to the (canonical) JSON model.
     *
     * @param body Unmarshalled ({@link HashMap}) representation of the order in XML, which will be automatically injected by the Camel Router.
     * @return Unmarshalled ({@link Order}) (canonical) representation of the order in JSON.
     */
    // Called via orderBean.
    @SuppressWarnings("unused")
    public Order xmlToCanonical(@Body final HashMap body) {
        final String id = readPropertyFromBody(body, "id");
        final String customerName = readPropertyFromBody(body, "customer", "name");
        final HashMap<String, Object> linesMap = readPropertyFromBody(body, "lines");
        OrderLine[] orderLines = null;
        if (linesMap != null) {
            final ArrayList<OrderLine> orderLinesList = new ArrayList<>();
            for (final String article : linesMap.keySet()) {
                final String price = readPropertyFromBody(linesMap, article, "price");
                final String amount = readPropertyFromBody(linesMap, article, "amount");
                orderLinesList.add(new OrderLine(article, amount == null ? 0 : Integer.parseInt(amount), price == null ? 0 : Double.parseDouble(price)));
            }
            orderLines = orderLinesList.toArray(new OrderLine[]{});
        }
        return new Order(id, customerName, orderLines);
    }

    /**
     * Helper method to read (nested) XML value.
     *
     * @param body       Unmarshalled ({@link HashMap}) representation of an XML snippet.
     * @param properties (Nested) element names that need to be retrieved. The latest property will be returned as value.
     * @param <T>        Return type.
     * @return Value of the latest (nested) property, or {@code null} if the property could not be found.
     */
    @SuppressWarnings("unchecked")
    private static <T> T readPropertyFromBody(final HashMap body, final String... properties) {
        HashMap currentElement = body;
        int counter = 0;
        for (final String property : properties) {
            final Object nextElement = currentElement.get(property);
            if (++counter == properties.length) {
                return (T) nextElement;
            } else if (nextElement instanceof HashMap) {
                currentElement = (HashMap) nextElement;
            }
        }
        return null;
    }
}
