package nl.whitehorses.camel.transformators;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import nl.whitehorses.camel.model.Order;
import nl.whitehorses.camel.model.OrderLine;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import java.util.ArrayList;

/**
 * Camel {@link Processor} to transform a {@link ArrayList} of Mongo {@link DBObject} instances to a {@link ArrayList}
 * of canonical {@link Order} objects.
 */
public class OrdersFromMongoTransformator implements Processor {

    /**
     * {@inheritDoc}
     *
     * @param exchange {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void process(final Exchange exchange) {
        final ArrayList<DBObject> body = exchange.getIn().getBody(ArrayList.class);
        final ArrayList<Order> canonicalList = new ArrayList<>();
        body.forEach(dbObject -> {
            final String id = (String) dbObject.get("_id");
            final String customerName = (String) dbObject.get("customerName");
            final Object dbOrderLines = dbObject.get("orderLines");
            OrderLine[] orderLines = null;
            if (dbOrderLines instanceof BasicDBList) {
                final ArrayList<OrderLine> orderLinesList = new ArrayList<>();
                ((BasicDBList) dbOrderLines).forEach(dbOrderLineObj -> {
                    final DBObject dbOrderLine = (DBObject) dbOrderLineObj;
                    final String article = (String) dbOrderLine.get("article");
                    final int amount = (Integer) dbOrderLine.get("amount");
                    final double price = (Double) dbOrderLine.get("price");
                    orderLinesList.add(new OrderLine(article, amount, price));
                });
                orderLines = orderLinesList.toArray(new OrderLine[]{});
            }
            canonicalList.add(new Order(id, customerName, orderLines));
        });
        exchange.getIn().setBody(canonicalList);
    }

}
