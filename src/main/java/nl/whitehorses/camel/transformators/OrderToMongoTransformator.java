package nl.whitehorses.camel.transformators;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import nl.whitehorses.camel.model.Order;
import nl.whitehorses.camel.model.OrderLine;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import java.util.Arrays;

/**
 * Camel {@link Processor} to transform a canonical {@link Order} instance to a Mongo {@link DBObject}.
 */
public class OrderToMongoTransformator implements Processor {

    /**
     * {@inheritDoc}
     *
     * @param exchange {@inheritDoc}
     */
    @Override
    public void process(final Exchange exchange) {
        final Order body = exchange.getIn().getBody(Order.class);
        final BasicDBObject mongoObject = new BasicDBObject("_id", body.getId());
        mongoObject.append("customerName", body.getCustomerName());
        final OrderLine[] orderLines = body.getOrderLines();
        if (orderLines != null && orderLines.length > 0) {
            final BasicDBList dbOrderLineList = new BasicDBList();
            Arrays.asList(orderLines).forEach(orderLine -> {
                final BasicDBObject orderLineObject = new BasicDBObject();
                orderLineObject.append("article", orderLine.getArticle());
                orderLineObject.append("amount", orderLine.getAmount());
                orderLineObject.append("price", orderLine.getPrice());
                dbOrderLineList.add(orderLineObject);
            });
            mongoObject.append("orderLines", dbOrderLineList);
        }
        exchange.getIn().setBody(mongoObject);
    }

}
