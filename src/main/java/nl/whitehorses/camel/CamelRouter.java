package nl.whitehorses.camel;

import nl.whitehorses.camel.beans.OrderBean;
import nl.whitehorses.camel.model.Order;
import nl.whitehorses.camel.transformators.OrderToMongoTransformator;
import nl.whitehorses.camel.transformators.OrdersFromMongoTransformator;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Camel routing of the application.
 */
@Component
public class CamelRouter extends RouteBuilder {

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure() {
        // (1): Register queue as dead letter channel.
        errorHandler(deadLetterChannel("activemq:queue:deadLetterChannel"));
        // (2) and (3): Register REST endpoints.
        rest("/orders")
            // (2): HTTP GET endpoint.
            .get()
                .produces("application/json")
                .to("direct:getOrders")
            // (3): HTTP POST endpoint.
            .post().consumes("application/xml")
                .produces("application/json")
                .to("direct:postOrder");
        // (4): Register file endpoint.
        from("file:{{order.file.location}}")
            .choice()
                .when(header("CamelFileName").isEqualTo("orders.json"))
                    // (5): Unmarshal JSON, split messages and dispatch messages to ActiveMQ queue.
                    .unmarshal().json(JsonLibrary.Jackson, Order[].class)
                        .split(body())
                            .to("activemq:queue:order")
                        .end()
                    .log("Finished dispatching orders to queue")
                .endChoice()
                .otherwise()
                    .log("Invalid file received: ${headers.CamelFileName}")
                .endChoice()
            .end();
        // (5): Register ActiveMQ queue subscription.
        from("activemq:queue:order")
                .setProperty("orderId", simple("${body.id}"))
                // (6): Transform canonical order model to MongoDB model using custom Processor.
                .process(new OrderToMongoTransformator())
                // (7): Insert order into MongoDB database.
                .to("mongodb:mongoClientBean?database={{mongodb.database}}&collection={{mongodb.collection}}&operation=insert")
                // (11): If the above step did not result in an error (which will trigger the errorHandler (1)), set body to "Successful".
                .transform(constant("Successful"))
                .log("Successfully inserted into collection: ${exchangeProperty[orderId]}");
        // (2): Implementation of HTTP GET endpoint.
        from("direct:getOrders")
            // (8): Get all orders from MongoDB database.
            .to("mongodb:mongoClientBean?database={{mongodb.database}}&collection={{mongodb.collection}}&operation=findAll")
            // (9): Transform MongoDB orders model to canonical model and marshal to JSON.
            .process(new OrdersFromMongoTransformator())
            .marshal().json(JsonLibrary.Jackson);
        // (3): Implementation of HTTP POST endpoint.
        from("direct:postOrder")
            // (10): Unmarshal JSON and transform message using a bean.
            .unmarshal().jacksonxml()
            .bean(OrderBean.class, "xmlToCanonical")
            // (5): Dispatch message to ActiveMQ queue.
            .to("activemq:queue:order")
            // (11): Transform payload to either successful or failure response using inline processor.
            .choice()
                .when(simple("${body} == 'Successful'"))
                    .process(exchange -> {
                        final HashMap<String, String> response = new HashMap<>();
                        response.put("message", "Successfully created order");
                        exchange.getIn().setBody(response);
                    })
                .endChoice()
                .otherwise()
                    .setHeader(Exchange.HTTP_RESPONSE_CODE, constant("500"))
                    .process(exchange -> {
                        final HashMap<String, String> response = new HashMap<>();
                        response.put("message", "Failed to insert order. Dispatched order to dead letter channel.");
                        exchange.getIn().setBody(response);
                    })
            .end()
            .marshal().json(JsonLibrary.Jackson);
    }
}
