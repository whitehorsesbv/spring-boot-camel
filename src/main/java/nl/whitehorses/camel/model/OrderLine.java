package nl.whitehorses.camel.model;

import java.io.Serializable;

/**
 * Canonical (JSON) representation of an order line.
 */
public class OrderLine implements Serializable {

    private String article;
    private int amount;
    private double price;

    /**
     * Default constructor.
     */
    public OrderLine() {
        super();
    }

    /**
     * Constructor.
     *
     * @param article Article name.
     * @param amount  Amount of units ordered.
     * @param price   Price per unit.
     */
    public OrderLine(final String article, final int amount, final double price) {
        this.article = article;
        this.amount = amount;
        this.price = price;
    }

    /**
     * Get article name.
     *
     * @return Article name.
     */
    public String getArticle() {
        return article;
    }

    /**
     * Set article name.
     *
     * @param article Article name.
     */
    public void setArticle(final String article) {
        this.article = article;
    }

    /**
     * Get amount of units ordered.
     *
     * @return Amount of units ordered.
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Set amount of units ordered.
     *
     * @param amount Amount of units ordered.
     */
    public void setAmount(final int amount) {
        this.amount = amount;
    }

    /**
     * Get price per unit.
     *
     * @return Price per unit.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Set price per unit.
     *
     * @param price Price per unit.
     */
    public void setPrice(final double price) {
        this.price = price;
    }

    /**
     * Read-only field to calculate the total price of the order line. This field will not be marshalled to the JSON
     * structure.
     *
     * @return Total price of the order line.
     */
    double getTotalPrice() {
        return amount * price;
    }
}
