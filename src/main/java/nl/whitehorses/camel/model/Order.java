package nl.whitehorses.camel.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Canonical (JSON) representation of an order.
 */
public class Order implements Serializable {

    private String id;
    private String customerName;
    private OrderLine[] orderLines;

    /**
     * Default constructor.
     */
    public Order() {
        super();
    }

    /**
     * Constructor.
     *
     * @param id           Order identifier.
     * @param customerName Customer name.
     * @param orderLines   Order lines.
     */
    public Order(final String id, final String customerName, final OrderLine[] orderLines) {
        this.id = id;
        this.customerName = customerName;
        this.orderLines = orderLines;
    }

    /**
     * Get order identifier.
     *
     * @return Order identifier.
     */
    public String getId() {
        return id;
    }

    /**
     * Set order identifier.
     *
     * @param id Order identifier.
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Get customer name.
     *
     * @return Customer name.
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Set customer name.
     *
     * @param customerName Customer name.
     */
    public void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }

    /**
     * Get order lines.
     *
     * @return Order lines.
     */
    public OrderLine[] getOrderLines() {
        return orderLines;
    }

    /**
     * Set order lines.
     *
     * @param orderLines Order lines.
     */
    public void setOrderLines(final OrderLine[] orderLines) {
        this.orderLines = orderLines;
    }

    /**
     * Read-only field to calculate the total price of all order lines.
     *
     * @return Total price of all order lines.
     */
    // Used to add totalPrice value when marshalling to XML.
    @SuppressWarnings("unused")
    public double getTotalPrice() {
        return Arrays.stream(getOrderLines())
                .mapToDouble(OrderLine::getTotalPrice)
                .sum();
    }

}
