# Builder stage
FROM maven:3.6.1 AS builder
COPY . ./spring-boot-camel
WORKDIR /spring-boot-camel
RUN mvn clean package

# Construct actual image
FROM tomcat:8.5.43

# Versions
ENV ACTIVEMQ_VERSION 5.15.9
ENV MONGODB_MAJOR 4.0
ENV MONGODB_VERSION 4.0.12

# Install ActiveMQ
RUN cd /usr/local && \
    wget http://apache.cs.uu.nl/activemq/$ACTIVEMQ_VERSION/apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz && \
    tar zxvf apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz && \
    rm apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz

# Install MongoDB
RUN wget -qO - https://www.mongodb.org/static/pgp/server-$MONGODB_MAJOR.asc | apt-key add - && \
    echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/$MONGODB_MAJOR main" | tee /etc/apt/sources.list.d/mongodb-org-$MONGODB_MAJOR.list && \
    apt-get update && \
    apt-get install -y \
        mongodb-org=$MONGODB_VERSION \
        mongodb-org-server=$MONGODB_VERSION \
        mongodb-org-shell=$MONGODB_VERSION \
        mongodb-org-mongos=$MONGODB_VERSION \
        mongodb-org-tools=$MONGODB_VERSION

# Create 'spring-boot-camel' database with 'orders' collection in MongoDB
RUN mkdir -p /data/db && \
    mongod --fork --logpath /var/log/mongod.log && \
    mongo spring-boot-camel --eval 'db.createCollection("orders")'

# Deploy spring-boot-camel application to Tomcat
COPY --from=builder /spring-boot-camel/target/spring-boot-camel.war $CATALINA_HOME/webapps/spring-boot-camel.war

# Create directory to receive orders via file
RUN mkdir -p /usr/local/share/orders

# Start MongoDB, ActiveMQ and Tomcat
CMD mongod --fork --logpath /var/log/mongod.log & \
    nohup /usr/local/apache-activemq-5.15.9/bin/activemq start > /var/log/activemq.log 2>&1 & \
    sh $CATALINA_HOME/bin/catalina.sh run
